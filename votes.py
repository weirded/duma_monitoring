# coding: utf-8
u"""всё связанное с законами"""
import json
from os.path import exists
from os import environ as env
from clients import JSONClient


def fetch_votes(client, filename='data/votes.json'):
    u"""тянет все голосования"""
    count = int(client.vote_search().get('totalCount'))
    limit = 100
    votes = list()
    for page in range(1, count / limit + 1):
        print "fetch {0}/{1} vote".format(page, count / limit)
        data = client.vote_search(page=page, limit=limit)
        votes.extend(data['votes'])
    with open(filename, 'w') as votes_file:
        json.dump(votes, votes_file, indent=4)


def fetch_votes_full_data(client, filename='data/votes.json'):
    vote_ids = [x['id'] for x in json.load(open(filename))]
    vote_ids_count = len(vote_ids)
    for n, vote_id in enumerate(vote_ids, 1):
        vote_filename = 'data/votes/{0}'.format(vote_id)
        if exists(vote_filename):
            continue
        print 'fetch {0}/{1} vote -> {2}'.format(n, vote_ids_count, vote_filename)
        data = client.vote(vote_id)
        with open(vote_filename, 'w') as vote_file:
            json.dump(data, vote_file, indent=4)


def main():
    client = JSONClient(env['API_TOKEN'], env['APP_TOKEN'])
    if not exists('data/votes.json'):
        fetch_votes(client)
    fetch_votes_full_data(client)

if __name__ == '__main__':
    main()
