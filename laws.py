# coding: utf-8
u"""всё связанное с законами"""
import json
from os import environ as env
from clients import JSONClient


def fetch_laws(client, filename='data/laws.json'):
    u"""тянет все рассмотренные законопроекты"""
    count = client.search(status=7, search_mode=2).get('count')
    limit = 20

    laws = list()
    for page in range(1, count / limit + 1):
        print "{0}/{1}".format(page, count / limit)
        data = client.search(status=7, search_mode=2, page=page)
        laws.extend(data['laws'])
    json.dump(laws, open(filename, 'w'), indent=4)


def main():
    client = JSONClient(env['API_TOKEN'], env['APP_TOKEN'])
    fetch_laws(client)


if __name__ == '__main__':
    main()
